<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Car;
use App\Models\Alert;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Disable foreign key checking because truncate() will fail
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');

      User::truncate();
      Car::truncate();
      Alert::truncate();
      factory(User::class, 10)->create();
      factory(Car::class, 20)->create();
      factory(Alert::class, 100)->create();
      // Enable it back
      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
