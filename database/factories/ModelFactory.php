<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Alert::class, function (Faker\Generator $faker) {
  $timezone = 'Europe/Bucharest';
  $dateFormat = 'Y-m-d';
  $max = 'now';

    return [
        'car_id' => mt_rand(1, 25),
        'alert_name' => $faker->randomElement(array('Rovinieta', 'RCA', 'TimPark', 'Schimb ulei'), $count = 1),
        'valid_from' => $faker->dateTimeThisYear($max = 'now', $timezone)->format($dateFormat),
        'valid_to' => $faker->dateTimeBetween($startDate = '+5 days', $endDate = '+365 days', $timezone)->format($dateFormat),
    ];
});

$factory->define(App\Models\Car::class, function (Faker\Generator $faker) {
  $makes =  array ('BMW','Audi','Mercedes', 'Dacia');
  $models = array (
    'BMW' => array (
      'X5',
      'Seria 3',
      'E46'
    ),
    'Audi' => array (
      'A4',
      'A3',
      'Q7'
    ),
    'Mercedes' => array (
      'C Class',
      'E Class',
      'GLC'
    ),
    'Dacia' => array (
      'Sandero',
      'Logan',
      'Duster'
    )
  );

  $plateNo = 'TM'. $faker->numberBetween($min = 01, $max = 99).mb_strtoupper($faker->randomLetter.$faker->randomLetter.$faker->randomLetter);
  $mk = $faker->randomElement($makes, $count = 1);
  $mdl = $faker->randomElement($models[$mk], $count = 1);

    return [
        'user_id' => mt_rand(1, 10),
        'plate_number' => $plateNo,
        'make' => $mk,
        'model' => $mdl,
        'type' => $faker->word.' '.$faker->word,
        'color' => $faker->colorName,
        'fuel_type' => $faker->randomElement(array('Diesel','Benzina'), $count = 1),
        'year' => $faker->year($max = 'now')
    ];
});
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    $hasher = app()->make('hash');
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->freeEmail,
        'password' => $hasher->make("secret")
    ];
});
