<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{

    protected $fillable = [
        'id', 'user_id', 'plate_number', 'make', 'model', 'type', 'color', 'fuel_type', 'year'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function alerts(){
        return $this->hasMany('App\Models\Alert');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
