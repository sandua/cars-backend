<?php
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Hash;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $fillable = [
        'id', 'name', 'email',
    ];

    //Use GLOBAL/LOCAL SCOPE for deleted_at (for non-admin parts)
    //https://laravel.com/docs/5.7/eloquent#query-scopes
    protected $hidden = [
        'password', 'created_at', 'remember_token', 'updated_at', 'deleted_at'
    ];

    public function getCars() {
      return $this->cars()->where('user_id', 1)->get();
    }

    public function cars(){
        return $this->hasMany('App\Models\Car');
    }
}
