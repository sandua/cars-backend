<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{

    protected $fillable = [
        'id', 'car_id', 'alert_name', 'valid_from', 'valid_to', 'valid_duration_days'
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function car(){
        return $this->belongsTo('App\Models\Car');
    }
}
