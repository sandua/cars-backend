Alert<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use Illuminate\Http\Request;

class AlertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index() {

      $alerts = Alert::all();

      return response()->json($alerts);
    }

    public function create() {
      return __METHOD__.__CLASS__;
    }

    public function store(Request $request) {
      return __METHOD__.__CLASS__;
    }

    public function show(Alert $alert) {
      return __METHOD__.__CLASS__;
    }

    //Edit view, not used
    public function edit($alert) {
      $cc = Alert::find($alert);


      $html = "<pre>";
      $html .= print_r($cc);
      $html .= "</pre>";
      return $html;
    }

    public function update(Request $request, $alertId) {
      $alert = Alert::find($alertId);
      return dd($alert);
    }

    public function destroy(Alert $alert) {
      return __METHOD__.__CLASS__;
    }
}
