<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

//TODO: !IMPORTANT validate / check brute force
class AuthController extends Controller
{
  private $iss;
  /**
   * The request instance.
   *
   * @var \Illuminate\Http\Request
   */
  private $request;
  /**
   * Create a new controller instance.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return void
   */
  public function __construct(Request $request) {
    // $this->middleware('guest');

    $this->request = $request;
    $this->iss = Hash::make("auth-service");
  }

  /**
     * Create a new token.
     *
     * @param  \App\Models\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => $this->iss, // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];

        return JWT::encode($payload, config('services.jwt.secret'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @return mixed
     */
    public function authenticate() {

      try {
        //TODO Add phone auth same field as email
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required|min:6'
        ]);
      } catch(\Exception $e) {
          return $this->errorCode(['error' => "Email or password does not exist or does not match."], 400);
      }

      //SANITIZE INPUT
      $user = User::where('email', $this->request->input('email'))->first();
      if (!$user) {
            return $this->error(['error' => "Email and/or password does not exist or does not match."]);
      }

      if (Hash::check($this->request->input('password'), $user->password)) {
          return $this->success(['token' => $this->jwt($user)]);
      }

        // Bad Request response
        return $this->error(['error' => "Email / password does not exist or does not match."]);
    }
}
