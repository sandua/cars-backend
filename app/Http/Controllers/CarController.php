<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Car;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAlerts(Request $request) {
      //TODO Use eager loading here (unfunctional method)
      $alerts = Car::with('cars.alerts')->get();

      $alerts = $alerts->map(function ($alert) {
        return $alert;
      });

      return $this->success($alerts);
    }

    public function getUserCars(Request $request) {
      //TODO Use Laravel Eager loading
      $user =  User::find($request->payload->sub);

      $carrs = $user->cars()->where('user_id', $user->id)->get();

      return $this->success($carrs);

    }

    //Default
    public function index() {

      $cars = Car::all();

      return $this->success($cars);
    }

    public function create() {
      return __METHOD__.__CLASS__;
    }

    public function store(Request $request) {
      //You can use $user->addCar($request);
      return $request;
      return __METHOD__.__CLASS__;
    }

    public function show($carId) {

    }

    //Edit view, not used
    public function edit($carId) {
      $cc = Car::find($carId);
      $html = "<pre>";
      $html .= print_r($cc);
      $html .= "</pre>";
      return $html;
    }

    public function update(Request $request, $carId) {
      $car = Car::find($carId);
      return $this->success($car);
    }

    public function destroy($carId) {
      Log::info('Deleting car with ID: '.$carId);
      //Car::destroy($carId);
      return $this->success($carId);
    }
}
