<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Handle an incoming request and set right headers
 * CORS: Access-Control-Allow-Origin
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
 */
class Cors {

  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) {
    return $next($request)
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  }
}
