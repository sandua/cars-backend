<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

//TODO: !IMPORTANT validate / check brute force
class JwtMiddleware extends BaseMiddleware
{
    use ProvidesConvenienceMethods;

    public function handle($request, Closure $next, $guard = null)
    {
        try {
            $this->validate($request, ['token'  => 'required']);

            $token = $request->get('token');

            $decodedJWT = JWT::decode($token, config('services.jwt.secret'), ['HS256']);

            $request->payload = $decodedJWT;

            //validate iss here maybe

            // $user = User::find($credentials->sub);

            // Now let's put the user in the request class so that you can grab it from there
            // $request->auth = $user;

        } catch(ValidationException $e) {
            return $this->errorCode(['error' => 'Token not provided.'], 401);
        } catch(ExpiredException $e) {
            return $this->error(['error' => 'Provided token is expired.']);
        } catch(Exception $e) {
            return $this->error(['error' => 'An error while decoding token.']);
        }

        return $next($request);
    }
}
