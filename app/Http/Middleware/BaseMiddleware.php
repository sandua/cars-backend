<?php

namespace App\Http\Middleware;

class BaseMiddleware
{
  //TODO: DUPLICATE from Http/Controllers/Controller
  public function successCode($data, $code) {
   return response()->json(['data' => $data], $code);
  }
  //TODO: DUPLICATE from Http/Controllers/Controller
  public function success($data) {
   return $this->successCode($data, 200);
  }
  //TODO: DUPLICATE from Http/Controllers/Controller
  public function errorCode($message, $code){
      return response()->json(['message' => $message], $code);
  }
  //TODO: DUPLICATE from Http/Controllers/Controller
  public function error($message) {
    return $this->errorCode($message, 400);
  }
}
