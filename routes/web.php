<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
  return $router->app->version();
});

$router->group(['middleware' => 'cors'],
    function() use ($router) {
    //CARS - unprotected
    $router->get('/cars', 'CarController@index');

    $router->post('/cars', 'CarController@store');

    $router->get('/cars/create', 'CarController@create');

    $router->get('/cars/{carId}', 'CarController@show');

    $router->get('/cars/{carId}/edit', 'CarController@edit');

    $router->patch('/cars/{carId}', 'CarController@update');

    $router->get('/cars/delete/{carId}', 'CarController@destroy');

    //USERS
    $router->post('auth/login', 'AuthController@authenticate');

    $router->get('/users', 'UserController@index');

    $router->post('/users', 'UserController@store');

    $router->get('/users/create', 'UserController@create');

    $router->get('/users/{userId}', 'UserController@show');
    // Edit FORM (not used)
    $router->get('/users/{car}/edit', 'UserController@edit');

    $router->patch('/users/{userId}', 'UserController@update');

    $router->delete('/users/{userId}', 'UserController@destroy');

    // LOGIN
    $router->post('/login', 'AuthController@authenticate');
  }
);

$router->group(['middleware' => ['jwt.auth', 'cors']],
    function() use ($router) {
        $router->get('/mycars', 'CarController@getUserCars');
        $router->get('/myalerts', 'CarController@getAlerts');
    }
);

$app = $router->app;

// $stripe = $app->make('App\Billing\Stripe');
// $stripe = app('App\Billing\Stripe');
// dd($stripe);

//TODO: CHECK BELOW ROUTES, WERE COPY PASTE


// Users
// $app->get('/users/', 'UserController@index');
// $app->post('/users/', 'UserController@store');
// $app->get('/users/{user_id}', 'UserController@show');
// $app->put('/users/{user_id}', 'UserController@update');
// $app->delete('/users/{user_id}', 'UserController@destroy');
// // Cars
// $app->get('/cars','CarController@index');
// $app->post('/cars','CarController@store');
// $app->get('/cars/{car_id}','CarController@show');
// $app->put('/cars/{car_id}', 'CarController@update');
// $app->delete('/cars/{car_id}', 'CarController@destroy');
// // Alerts
// $app->get('/alerts', 'AlertController@index');
// $app->get('/alerts/{alert_id}', 'AlertController@show');
// // Alerts of a Car
// $app->get('/cars/{car_id}/alerts', 'CarAlertController@index');
// $app->post('/cars/{car_id}/alerts', 'CarAlertController@store');
// $app->put('/cars/{car_id}/alerts/{alert_id}', 'CarAlertController@update');
// $app->delete('/cars/{car_id}/alerts/{alert_id}', 'CarAlertController@destroy');
